-- Namespace
zoonami = {}

-- Mod path
local mod_path = minetest.get_modpath("zoonami")


--- Libraries to be imported into other files ---

-- Formspec
local fs = dofile(mod_path .. "/lua/formspec.lua")

-- Sounds
local sounds = dofile(mod_path .. "/lua/sounds.lua")

-- Type Chart
local types = dofile(mod_path .. "/lua/types.lua")

-- Move Stats
local move_stats = dofile(mod_path .. "/lua/move_stats.lua")

-- Item Stats
local item_stats = dofile(mod_path .. "/lua/item_stats.lua")

-- Groups
local group = dofile(mod_path .. "/lua/group.lua")

-- Biomes
local biomes = assert(loadfile(mod_path .. "/lua/biomes.lua"))(group)

-- Battle AI
local ai = assert(loadfile(mod_path .. "/lua/ai.lua"))(move_stats, types)

-- Monsters
local monsters = assert(loadfile(mod_path .. "/lua/monsters.lua"))(group, move_stats)

-- NPC Stats
local npc_stats = assert(loadfile(mod_path .. "/lua/npc_stats.lua"))(fs, group, monsters)

-- Mobs API
local mobs_api = assert(loadfile(mod_path .. "/lua/mobs_api.lua"))(group, monsters, npc_stats)


--- Main game files ---

-- Chat Commands
assert(loadfile(mod_path .. "/lua/chat_commands.lua"))(fs, monsters, move_stats)

-- Craft Items
assert(loadfile(mod_path .. "/lua/craft_items.lua"))(monsters)

-- Nodes
assert(loadfile(mod_path .. "/lua/nodes.lua"))(monsters, sounds)

-- Tools
dofile(mod_path .. "/lua/tools.lua")

-- Vending Machines
assert(loadfile(mod_path .. "/lua/vending_machines.lua"))(fs, sounds)

-- Computer
assert(loadfile(mod_path .. "/lua/computer.lua"))(fs, monsters, move_stats, sounds)

-- Trading Machine
assert(loadfile(mod_path .. "/lua/trading_machine.lua"))(fs, monsters, move_stats, sounds)

-- Monster Spawner
assert(loadfile(mod_path .. "/lua/monster_spawner.lua"))(fs, group, monsters, sounds)

-- Crafting
assert(loadfile(mod_path .. "/lua/crafting.lua"))(fs, group)

-- Mobs
assert(loadfile(mod_path .. "/lua/mobs.lua"))(mobs_api, monsters, npc_stats)

-- Battle
dofile(mod_path .. "/lua/battle.lua")

-- Battle
assert(loadfile(mod_path .. "/lua/battle.lua"))(ai, biomes, fs, item_stats, monsters, move_stats, types)

-- Backpack
assert(loadfile(mod_path .. "/lua/backpack.lua"))(fs, monsters, move_stats)

-- Guide Book
assert(loadfile(mod_path .. "/lua/guide_book.lua"))(fs)

-- Monster Journal
assert(loadfile(mod_path .. "/lua/monster_journal.lua"))(fs, monsters, move_stats)

-- Move Journal
assert(loadfile(mod_path .. "/lua/move_journal.lua"))(fs, move_stats)

-- Move Books
assert(loadfile(mod_path .. "/lua/move_books.lua"))(fs, monsters, move_stats)

-- Berry Juice
assert(loadfile(mod_path .. "/lua/berry_juice.lua"))(fs, monsters)

-- Candy
assert(loadfile(mod_path .. "/lua/candy.lua"))(fs, monsters)

-- Give Initial Stuff
dofile(mod_path .. "/lua/give_initial_stuff.lua")

-- Mesecons
dofile(mod_path .. "/lua/mesecons.lua")

-- Mapgen
minetest.register_on_mods_loaded(function()
    assert(loadfile(mod_path .. "/lua/mapgen.lua"))(biomes, group)
end)
