-- Monster Repellent
minetest.register_tool("zoonami:monster_repellent", {
	description = "Monster Repellent",
	inventory_image = "zoonami_monster_repellent.png",
	tool_capabilities = {
		full_punch_interval = 1,
		max_drop_level = 0,
		groupcaps = {},
		damage_groups = {},
	},
	sound = {},
	groups = {},
	on_use = function(itemstack, user, pointed_thing)
		if not user or not user:is_player() then return end
		local player_name = user:get_player_name()
		local player_pos = user:get_pos()
		local objs = minetest.get_objects_inside_radius(player_pos, 20)
		for _,obj in pairs(objs) do
			if obj and not obj:is_player() then
				local luaent = obj:get_luaentity()
				if luaent and luaent.name:find('zoonami:') then
					if luaent._type == "monster" and not luaent._showcase_id then
						luaent.object:remove()
					end
				end
			end
		end
		minetest.sound_play("zoonami_monster_repellent", {to_player = player_name, gain = 0.5}, true)
		itemstack:add_wear(5000)
		return itemstack
	end
})
